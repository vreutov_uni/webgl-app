# WebGL App

# Demo
https://vreutov-webgl-app.herokuapp.com/

# Installation
```
npm install
```

# Local
## Dev-server w/ hot-reloading
Starts by default on http://localhost:8080 
```
npm run dev
```

## Prod-server
Starts by default on http://localhost:8081
```
npm run heroku-postbuild
npm run start
```
