/* eslint-disable @typescript-eslint/no-var-requires */
/* eslint-disable no-console */

const express = require('express');
const path = require('path');
const http = require('http');
const io = require('socket.io');

const port = process.env.PORT || 8081;
const app = express();
const server = http.createServer(app);
const ioServer = io(server);

const distdir = path.resolve(__dirname, 'dist');
app.use(express.static(distdir));

app.get('/', (req, res) => {
  res.sendFile(path.resolve(distdir, 'index.html'));
});

const sockets = new Map();

ioServer.on('connection', (socket) => {
  console.log(`Connected ${socket.id}`);
  sockets.set(socket.id, socket);

  socket.on('send_quaternion', (target, rotation) => {
    const receiver = sockets.get(target);
    if (receiver) {
      receiver.emit('send_quaternion', rotation);
    } else {
      console.log(`${target} not found on server`);
      socket.emit('target_gone');
    }
  });

  socket.on('disconnect', () => {
    console.log(`Disconnected ${socket.id}`);
    sockets.delete(socket.id);
  });
});

server.listen(port, () => {
  console.log(`Listening on port ${port}...`);
});
