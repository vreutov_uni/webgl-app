attribute vec4 aPosition;
attribute vec4 aColor;

uniform mat4 uModelViewProjection;

varying lowp vec4 vColor;

void main() {
    gl_Position = uModelViewProjection * aPosition;
    vColor = aColor;
}
