import { GLObject } from '../../../base/object';

import vsSource from './shaders/simple.vert';
import fsSource from './shaders/simple.frag';

export abstract class SimpleObject extends GLObject {
  getVS(): string {
    return vsSource;
  }

  getFS(): string {
    return fsSource;
  }

  getInspectingFS(): string {
    return fsSource;
  }
}
