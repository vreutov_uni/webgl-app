import { BufferInfo, createBufferInfoFromArrays } from 'twgl.js';
import { TexturedObject } from '../textured';

import { verticlesToNDC, computeBounds } from '../../../utils';

const TYPES = {
  VERTEX: 'v',
  NORMAL: 'vn',
  TEXTURE: 'vt',
  FACE: 'f',
};

export abstract class ObjModel extends TexturedObject {
  constructor() {
    super();
    this.setTexture(this.getTexture());
  }

  protected abstract getTexture(): string;

  protected abstract getObjFile(): string;

  async createBufferInfo(): Promise<BufferInfo> {
    const verticles: number[] = [];
    const normals: number[] = [];
    const texcoord: number[] = [];
    const indicies: number[] = [];

    const oVerticles: number[][] = [];
    const oNormals: number[][] = [];
    const oTexcoords: number[][] = [];

    const obj = this.getObjFile();
    obj.split('\n').forEach((line) => {
      const [type, ...rest] = line.split(' ').filter((x) => x.trim().length > 0);
      switch (type) {
        case TYPES.VERTEX:
          oVerticles.push(rest.map((x) => Number(x)));
          break;
        case TYPES.NORMAL:
          oNormals.push(rest.map((x) => Number(x)));
          break;
        case TYPES.TEXTURE: {
          const [u, v] = rest.map((x) => Number(x)).slice(0, 2);
          oTexcoords.push([u, 1 - v]);
          break;
        }
        case TYPES.FACE: {
          rest.forEach((face, i) => {
            const [v, vt, vn] = face.split('/').map((x) => Number(x));
            verticles.push(...oVerticles[v - 1]);
            texcoord.push(...oTexcoords[vt - 1]);
            normals.push(...oNormals[vn - 1]);

            if (i < 3) {
              indicies.push((verticles.length / 3) - 1);
            }
            if (i === 3) {
              indicies.push((verticles.length / 3) - 2);
              indicies.push((verticles.length / 3) - 1);
              indicies.push((verticles.length / 3) - 4);
            } else if (i > 3) {
              throw new Error('Polygons with more than 4 verticles are unsupported');
            }
          });
          break;
        }
        default:
          // nothing to do here, skip
          break;
      }
    });

    verticlesToNDC(verticles);
    this.bounds = computeBounds(verticles);

    const arrays = {
      aPosition: {
        numComponents: 3,
        data: verticles,
      },
      aNormal: {
        numComponents: 3,
        data: normals,
      },
      aTexcoord: {
        numComponents: 2,
        data: texcoord,
      },
      aColor: {
        numComponents: 3,
        data: normals,
      },
      indices: {
        numComponents: 3,
        data: indicies,
      },
    };

    return createBufferInfoFromArrays(this.gl, arrays);
  }
}
