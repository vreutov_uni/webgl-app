#pragma glslify: transpose = require(glsl-transpose)

uniform mat4 uModel;
uniform mat4 uModelViewProjection;
uniform mat4 uModelInverseTranspose;
uniform mat4 uViewInverse;

uniform vec3 uLightWorldPos;

attribute vec4 aPosition;
attribute vec3 aNormal;
attribute vec2 aTexcoord;
attribute vec3 aTangent;

varying vec4 vPosition;
varying vec2 vTexcoord;
varying vec3 vSurfaceToLight;
varying vec3 vSurfaceToView;

void main() {
  vTexcoord = aTexcoord;
  vPosition = uModelViewProjection * aPosition;
  vec3 T = (uModelInverseTranspose * vec4(aTangent, 0)).xyz;
  vec3 N = (uModelInverseTranspose * vec4(aNormal, 0)).xyz;
  vec3 B = cross(N, T);
  mat3 TBN = transpose(mat3(T, B, N));

  vSurfaceToLight = uLightWorldPos - (uModel * aPosition).xyz;
  vSurfaceToLight = normalize(TBN * vSurfaceToLight);

  vSurfaceToView = (uViewInverse[3] - (uModel * aPosition)).xyz;
  vSurfaceToView = normalize(TBN * vSurfaceToView);

  gl_Position = vPosition;
}
