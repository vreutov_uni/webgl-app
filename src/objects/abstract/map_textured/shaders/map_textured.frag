precision mediump float;

varying vec4 vPosition;
varying vec2 vTexcoord;
varying vec3 vSurfaceToLight;
varying vec3 vSurfaceToView;

uniform sampler2D uTexture;
uniform sampler2D uNormalMap;

uniform vec4 uLightColor;
uniform vec4 uAmbient;
uniform vec4 uSpecular;

uniform float uShininess;
uniform float uSpecularFactor;

vec4 lit(float l ,float h, float m) {
  return vec4(1.0, max(l, 0.0), (l > 0.0) ? pow(max(0.0, h), m) : 0.0, 1.0);
}

void main() {
  vec4 diffuseColor = texture2D(uTexture, vTexcoord);
  vec3 normal = texture2D(uNormalMap, vTexcoord).rgb;
  normal = normalize(normal * 2.0 - 1.0);
  
  vec3 surfaceToLight = normalize(vSurfaceToLight);
  vec3 surfaceToView = normalize(vSurfaceToView);
  vec3 halfVector = normalize(surfaceToLight + surfaceToView);
  vec4 litR = lit(
    dot(normal, surfaceToLight),
    dot(normal, halfVector),
    uShininess
  );
  vec4 outColor = vec4(
    (
      uLightColor * (diffuseColor * litR.y + diffuseColor * uAmbient + uSpecular * litR.z * uSpecularFactor)
    ).rgb,
    diffuseColor.a
  );
  gl_FragColor = outColor;
}
