import { createTexture, v3 } from 'twgl.js';
import { GLObject, Uniforms } from '../../../base/object';

import vsSource from './shaders/map_textured.vert';
import fsSource from './shaders/map_textured.frag';

export abstract class MapTexturedObject extends GLObject {
  getVS(): string {
    return vsSource;
  }

  getFS(): string {
    return fsSource;
  }

  getInspectingFS(): string {
    return fsSource;
  }

  private texture = 'i-dont-exist';

  private normalMap = 'i-dont-exist';

  constructor(texture: string, normalMap: string) {
    super();
    this.texture = texture;
    this.normalMap = normalMap;
  }

  createUniforms(): Uniforms {
    return {
      uTexture: createTexture(this.gl, { src: this.texture }),
      uNormalMap: createTexture(this.gl, { src: this.normalMap }),
    };
  }

  computeTangents(verticles: number[][], texcoords: number[][], indicies: number[]): number[][] {
    const tangents = verticles.map(() => [0, 0, 0]);

    for (let i = 0; i < indicies.length; i += 3) {
      const idx0 = indicies[i + 0];
      const idx1 = indicies[i + 1];
      const idx2 = indicies[i + 2];

      const v0 = verticles[idx0];
      const v1 = verticles[idx1];
      const v2 = verticles[idx2];

      const uv0 = [...texcoords[idx0], 0.0];
      const uv1 = [...texcoords[idx1], 0.0];
      const uv2 = [...texcoords[idx2], 0.0];

      const deltaPos1 = v3.subtract(v1, v0);
      const deltaPos2 = v3.subtract(v2, v0);

      const deltaUV1 = v3.subtract(uv1, uv0);
      const deltaUV2 = v3.subtract(uv2, uv0);

      const r = 1.0 / (deltaUV1[0] * deltaUV2[1] - deltaUV1[1] * deltaUV2[0]);
      const tangent = Array.from(v3.mulScalar(
        v3.subtract(
          v3.mulScalar(deltaPos1, deltaUV2[1]),
          v3.mulScalar(deltaPos2, deltaUV1[1]),
        ), r,
      ));

      tangents[idx0] = tangent;
      tangents[idx1] = tangent;
      tangents[idx2] = tangent;
    }

    return tangents;
  }
}
