uniform mat4 uModel;
uniform mat4 uModelViewProjection;
uniform mat4 uModelInverseTranspose;
uniform mat4 uViewInverse;

uniform vec3 uLightWorldPos;

attribute vec4 aPosition;
attribute vec3 aNormal;
attribute vec2 aTexcoord;

varying vec4 vPosition;
varying vec2 vTexcoord;
varying vec3 vNormal;
varying vec3 vSurfaceToLight;
varying vec3 vSurfaceToView;

void main() {
  vTexcoord = aTexcoord;
  vPosition = uModelViewProjection * aPosition;
  vNormal = (uModelInverseTranspose * vec4(aNormal, 0)).xyz;
  vSurfaceToLight = uLightWorldPos - (uModel * aPosition).xyz;
  vSurfaceToView = (uViewInverse[3] - (uModel * aPosition)).xyz;
  gl_Position = vPosition;
}
