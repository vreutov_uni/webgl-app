import { createTexture } from 'twgl.js';
import { GLObject, Uniforms } from '../../../base/object';

import vsSource from './shaders/textured.vert';
import fsSource from './shaders/textured.frag';
import fsNoLightSource from './shaders/no_lighting.frag';

export abstract class TexturedObject extends GLObject {
  getVS(): string {
    return vsSource;
  }

  getFS(): string {
    return fsSource;
  }

  getInspectingFS(): string {
    return fsNoLightSource;
    // return fsSource;
  }

  private texture = 'i-dont-exist';

  setTexture(texture: string): void{
    this.texture = texture;
  }

  createUniforms(): Uniforms {
    return {
      uTexture: createTexture(this.gl, { src: this.texture }),
    };
  }
}
