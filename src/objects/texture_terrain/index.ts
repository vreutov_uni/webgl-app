import { BufferInfo, createBufferInfoFromArrays, v3 } from 'twgl.js';
import { TexturedObject } from '../abstract/textured';

export class TextureTerrain extends TexturedObject {
  meshSize: number;

  terrainImage: string;

  textureSize: number;

  private imageData!: ImageData;

  constructor(meshSize: number, terrainImage: string, textureImage: string, textureSize: number) {
    super();
    this.setTexture(textureImage);
    this.meshSize = meshSize;
    this.terrainImage = terrainImage;
    this.textureSize = textureSize;
    this.canBeInspected = false;
  }

  async loadImageData(terrain: string): Promise<ImageData> {
    return new Promise((resolve, reject): void => {
      const img = new Image();
      img.onload = (): void => {
        const canvas = document.createElement('canvas');
        canvas.width = img.width;
        canvas.height = img.height;

        const context = canvas.getContext('2d') as CanvasRenderingContext2D;
        context.drawImage(img, 0, 0);
        resolve(context.getImageData(0, 0, canvas.width, canvas.height));
      };
      img.onerror = reject;
      img.src = terrain;
    });
  }

  getZ(x: number, y: number): number {
    const scaleY = this.imageData.height / this.meshSize;
    const scaleX = this.imageData.width / this.meshSize;

    const canvasX = Math.round(x * scaleX);
    const canvasY = this.imageData.height - Math.round(y * scaleY) - 1;

    return this.imageData.data[4 * canvasY * this.imageData.width + 4 * canvasX] / 255;
  }

  async createBufferInfo(): Promise<BufferInfo> {
    this.imageData = await this.loadImageData(this.terrainImage);

    const normals = [];
    const texcoord = [];
    const indices = [];

    const verticles = [];

    for (let i = 0; i < this.meshSize; i += 1) {
      for (let j = 0; j < this.meshSize; j += 1) {
        const x = ((2 * j) / (this.meshSize - 1)) - 1;
        const y = ((2 * i) / (this.meshSize - 1)) - 1;

        verticles.push([x, y, this.getZ(j, i)]);
        normals.push([0, 0, 0]);
        texcoord.push(j / this.textureSize, i / this.textureSize);

        if (j !== this.meshSize - 1) {
          const index = i * this.meshSize + j;

          if (i !== this.meshSize - 1) {
            indices.push(index, index + 1, index + this.meshSize);
          }

          if (i !== 0) {
            indices.push(index - this.meshSize + 1, index + 1, index);
          }
        }
      }
    }

    for (let i = 0; i < indices.length; i += 3) {
      const vert1 = verticles[indices[i]];
      const vert2 = verticles[indices[i + 1]];
      const vert3 = verticles[indices[i + 2]];

      const v21 = v3.create();
      v3.subtract(vert2, vert1, v21);
      const v32 = v3.create();
      v3.subtract(vert3, vert2, v32);
      const normal = v3.create();
      v3.cross(v21, v32, normal);
      v3.normalize(normal, normal);

      for (let j = 0; j < 3; j += 1) {
        const vNormal = normals[indices[i + j]];
        v3.add(normal, vNormal, vNormal);
        v3.normalize(vNormal, vNormal);
      }
    }

    const arrays = {
      aPosition: {
        numComponents: 3,
        data: ([] as number[]).concat(...verticles),
      },
      aNormal: {
        numComponents: 3,
        data: ([] as number[]).concat(...normals),
      },
      aTexcoord: {
        numComponents: 2,
        data: texcoord,
      },
      aColor: {
        numComponents: 4,
        data: ([] as number[]).concat(...normals.map((n) => [...n, 1])),
      },
      indices: {
        numComponents: 3,
        data: indices,
      },
    };
    return createBufferInfoFromArrays(this.gl, arrays);
  }
}
