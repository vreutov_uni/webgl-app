import {
  createBufferInfoFromArrays,
  Arrays,
  FullArraySpec,
  BufferInfo,
} from 'twgl.js';
import {
  createRandomColor,
} from '../../utils';
import { SimpleObject } from '../abstract/simple';

type zProvider = (x: number, y: number) => number;

function defaultGetZ(): number {
  return 0;
}

export class RandomColorTerrain extends SimpleObject {
  meshSize: number;

  getZ: zProvider;

  arrays: Arrays;

  constructor(meshSize: number, getZ?: zProvider) {
    super();
    this.meshSize = meshSize;
    this.getZ = getZ || defaultGetZ;
    this.arrays = {};
  }

  createPositions(): number[] {
    const positions = [];

    for (let i = 0; i < this.meshSize; i += 1) {
      for (let j = 0; j < this.meshSize; j += 1) {
        const x = ((2 * j) / (this.meshSize - 1)) - 1;
        const y = ((2 * i) / (this.meshSize - 1)) - 1;

        positions.push(x, y, this.getZ(j, i));
      }
    }

    return positions;
  }

  createColors(): number[] {
    const colors = [];

    for (let i = 0; i < this.meshSize; i += 1) {
      for (let j = 0; j < this.meshSize; j += 1) {
        colors.push(...createRandomColor());
      }
    }

    return colors;
  }

  createIndicies(): number[] {
    const indices = [];

    for (let i = 0; i < this.meshSize; i += 1) {
      for (let j = 0; j < this.meshSize - 1; j += 1) {
        const index = i * this.meshSize + j;

        if (i !== this.meshSize - 1) {
          indices.push(index, index + 1, index + this.meshSize);
        }

        if (i !== 0) {
          indices.push(index - this.meshSize + 1, index + 1, index);
        }
      }
    }

    return indices;
  }

  public recreatePositions(): void {
    (this.arrays.aPosition as FullArraySpec).data = this.createPositions();
    this.bufferInfo = createBufferInfoFromArrays(this.gl, this.arrays);
  }

  async createBufferInfo(): Promise<BufferInfo> {
    this.arrays = {
      aPosition: {
        numComponents: 3,
        data: this.createPositions(),
      },
      aNormal: {
        numComponents: 3,
        data: this.createPositions(),
      },
      aColor: {
        numComponents: 4,
        data: this.createColors(),
      },
      indices: {
        numComponents: 3,
        data: this.createIndicies(),
      },
    };
    return createBufferInfoFromArrays(this.gl, this.arrays);
  }
}
