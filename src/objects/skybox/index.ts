import { createTexture, BufferInfo, createBufferInfoFromArrays } from 'twgl.js';
import { GLObject, Uniforms } from '../../base/object';

import vsSource from './shaders/skybox.vert';
import fsSource from './shaders/skybox.frag';

type Cubemap = {
  posx: string;
  negx: string;
  posy: string;
  negy: string;
  posz: string;
  negz: string;
}

export class Skybox extends GLObject {
  getVS(): string {
    return vsSource;
  }

  getFS(): string {
    return fsSource;
  }

  texture: Cubemap

  constructor(texture: Cubemap) {
    super();
    this.texture = texture;
    this.canBeInspected = false;
  }

  createUniforms(): Uniforms {
    return {
      uTexture: createTexture(this.gl, {
        target: this.gl.TEXTURE_CUBE_MAP,
        src: [
          this.texture.posx,
          this.texture.negx,
          this.texture.posy,
          this.texture.negy,
          this.texture.posz,
          this.texture.negz,
        ],
      }),
    };
  }

  async createBufferInfo(): Promise<BufferInfo> {
    const positions = [
      -1.0, -1.0, 1.0,
      1.0, -1.0, 1.0,
      1.0, 1.0, 1.0,
      -1.0, 1.0, 1.0,
      -1.0, -1.0, -1.0,
      1.0, -1.0, -1.0,
      1.0, 1.0, -1.0,
      -1.0, 1.0, -1.0,
    ];

    const indices = [
      0, 3, 1, 1, 3, 2,
      0, 4, 7, 7, 3, 0,
      1, 2, 6, 6, 5, 1,
      5, 6, 7, 7, 4, 5,
      3, 7, 6, 6, 2, 3,
      0, 1, 5, 5, 4, 0,
    ];

    const arrays = {
      aPosition: {
        numComponents: 3,
        data: positions,
      },
      indices: {
        numComponents: 3,
        data: indices,
      },
    };

    return createBufferInfoFromArrays(this.gl, arrays);
  }
}
