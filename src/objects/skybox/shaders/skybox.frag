precision mediump float;

uniform samplerCube uTexture;

varying vec3 vTexcoord;

void main() {
  gl_FragColor = textureCube(uTexture, vTexcoord);
}
