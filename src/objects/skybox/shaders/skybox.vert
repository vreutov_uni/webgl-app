attribute vec3 aPosition;

uniform mat4 uModelViewProjection;

varying vec3 vTexcoord;

void main() {
  vTexcoord = vec3(aPosition.x, aPosition.y, aPosition.z);
  gl_Position = uModelViewProjection * vec4(aPosition, 1.0);
}
