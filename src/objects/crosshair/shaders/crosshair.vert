precision mediump float;

attribute vec4 aPosition;

uniform mat4 uModelViewProjection;

void main() {
    gl_Position = uModelViewProjection * aPosition;
}
