import { BufferInfo, createBufferInfoFromArrays } from 'twgl.js';
import { GLObject, Uniforms } from '../../base/object';

import vsSource from './shaders/crosshair.vert';
import fsSource from './shaders/crosshair.frag';


export class Crosshair extends GLObject {
  getVS(): string {
    return vsSource;
  }

  getFS(): string {
    return fsSource;
  }

  constructor() {
    super();
    this.isOverlay = true;
  }

  async createBufferInfo(): Promise<BufferInfo> {
    const size = 0.02;
    const width = 0.003;

    const arrays = {
      aPosition: {
        numComponents: 2,
        // somehow doubling makes it symmetric
        data: [
          // horizontal
          -size, width,
          -size, -width,
          size, width,
          size, -width,
          // vertical
          -width, size,
          width, size,
          -width, -size,
          width, -size,
        ],
      },
      indices: {
        numComponents: 3,
        data: [
          0, 2, 1, 1, 2, 3,
          4, 7, 6, 4, 5, 7,
        ],
      },
    };

    return createBufferInfoFromArrays(this.gl, arrays);
  }

  createUniforms(): Uniforms {
    return {
      uColor: [1, 1, 1, 1],
    };
  }
}
