
import {
  BufferInfo,
  createBufferInfoFromArrays,
} from 'twgl.js';
import { MapTexturedObject } from '../abstract/map_textured';
import Texture from './textures/brickwall.jpg';
import NormalMap from './textures/brickwall_normal.jpg';

export class Brickwall extends MapTexturedObject {
  constructor() {
    super(Texture, NormalMap);
  }

  async createBufferInfo(): Promise<BufferInfo> {
    const verticles = [
      // front
      [-1.0, 1.0, 1.0],
      [-1.0, -1.0, 1.0],
      [1.0, -1.0, 1.0],
      [1.0, 1.0, 1.0],

      // right
      [1.0, -1.0, -1.0],
      [1.0, 1.0, -1.0],
      [1.0, 1.0, 1.0],
      [1.0, -1.0, 1.0],
    ];

    const texcoords = [
      // front
      [0.0, 1.0],
      [0.0, 0.0],
      [1.0, 0.0],
      [1.0, 1.0],

      // right
      [0.0, 1.0],
      [0.0, 0.0],
      [1.0, 0.0],
      [1.0, 1.0],
    ];

    const normals = [
      // front
      [0.0, 0.0, 1.0],
      [0.0, 0.0, 1.0],
      [0.0, 0.0, 1.0],
      [0.0, 0.0, 1.0],

      // right
      [1.0, 0.0, 0.0],
      [1.0, 0.0, 0.0],
      [1.0, 0.0, 0.0],
      [1.0, 0.0, 0.0],
    ];

    const indices = [
      0, 1, 2, 0, 2, 3,
      4, 5, 6, 4, 6, 7,
    ];

    const tangents = this.computeTangents(verticles, texcoords, indices);

    const arrays = {
      aPosition: {
        numComponents: 3,
        data: ([] as number[]).concat(...verticles),
      },
      aNormal: {
        numComponents: 3,
        data: ([] as number[]).concat(...normals),
      },
      aTangent: {
        numComponents: 3,
        data: ([] as number[]).concat(...tangents),
      },
      aTexcoord: {
        numComponents: 2,
        data: ([] as number[]).concat(...texcoords),
      },
      aColor: {
        numComponents: 4,
        data: ([] as number[]).concat(...normals.map((n) => [...n, 1])),
      },
      indices: {
        numComponents: 3,
        data: indices,
      },
    };
    return createBufferInfoFromArrays(this.gl, arrays);
  }
}
