import { ObjModel } from '../abstract/obj_model';

import Obj from '../../models/andy/andy.obj';
import Tex from '../../models/andy/andy.png';

export class Andy extends ObjModel {
  protected getTexture(): string {
    return Tex;
  }

  protected getObjFile(): string {
    return Obj;
  }
}
