import { ObjModel } from '../abstract/obj_model';

import SpongeObj from '../../models/sponge/sponge.obj';
import SpongeTex from '../../models/sponge/sponge.png';

export class Sponge extends ObjModel {
  protected getTexture(): string {
    return SpongeTex;
  }

  protected getObjFile(): string {
    return SpongeObj;
  }
}
