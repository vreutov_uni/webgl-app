import { ObjModel } from '../abstract/obj_model';

import Obj from '../../models/penguin/penguin.obj';
import Tex from '../../models/penguin/penguin.png';

export class Penguin extends ObjModel {
  protected getTexture(): string {
    return Tex;
  }

  protected getObjFile(): string {
    return Obj;
  }
}
