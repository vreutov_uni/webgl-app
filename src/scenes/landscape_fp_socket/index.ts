import { FirstPersonLandscapeScene } from '../landscape_fp';

export class FirstPersonLandscapeWithSocketScene extends FirstPersonLandscapeScene {
  private socket: SocketIOClient.Socket

  private socketIdDiv?: HTMLDivElement;

  constructor(socket: SocketIOClient.Socket, gl: WebGLRenderingContext, overlay?: HTMLDivElement) {
    super(gl, overlay);
    this.socket = socket;
  }

  protected setupInspectionCallbacks(): void {
    this.socket.on('reconnect', () => {
      this.updateSocketIdUi();
    });

    this.socket.on('send_quaternion', (quaternionMsg: string) => {
      if (this.inspectedObject && this.inspectedObject.orbitalCamera) {
        const quaternion = JSON.parse(quaternionMsg);
        this.inspectedObject.orbitalCamera.rotateWithQuaternion(quaternion);
      }
    });
  }

  protected createOverlay(): void {
    super.createOverlay();

    const overlay = this.overlay as HTMLDivElement;
    if (overlay) {
      this.socketIdDiv = document.createElement('div');
      overlay.appendChild(this.socketIdDiv);
      this.updateSocketIdUi();
    }
  }

  protected updateSocketIdUi(): void {
    if (this.socketIdDiv) {
      this.socketIdDiv.innerText = `Socket id = ${this.socket.id}`;
    }
  }
}
