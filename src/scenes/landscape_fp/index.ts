import { v3 } from 'twgl.js';
import { LandscapeScene } from '../landscape';
import { FirstPersonCamera } from '../../cameras/first_person_camera';
import { Camera } from '../../base/camera';
import { TextureTerrain } from '../../objects/texture_terrain';
import { Cube } from '../../objects/cube';
import { Sponge } from '../../objects/sponge';
import { Penguin } from '../../objects/penguin';
import { Andy } from '../../objects/andy';
import { Crosshair } from '../../objects/crosshair';

export class FirstPersonLandscapeScene extends LandscapeScene {
  private pressedKeys: {
    [key: number]: boolean;
  } = {}

  private keyCodes = {
    forward: 87,
    backward: 83,
    left: 65,
    right: 68,
    e: 101,
    f: 102,
    shift: 16,
  }

  private movementSpeed = 0.07;

  private rotationSpeed = 0.01;

  mouseLocked = false;

  private drag = {
    isDragging: false,
    startX: 0,
    startY: 0,
  }

  private cameraHelp!: HTMLDivElement;

  protected createCamera(): Camera {
    return new FirstPersonCamera(
      1,
      (pos, yOffset): v3.Vec3 => this.positionConstraint(pos, yOffset),
    );
  }

  private getMovementSpeed(): number {
    if (this.pressedKeys[this.keyCodes.shift]) {
      return this.movementSpeed * 2;
    }
    return this.movementSpeed;
  }

  private mapLocalToTerrain(x: number): number {
    return (x / this.terrainScale + 1) * (this.meshSize / 2);
  }

  protected positionConstraint(pos: v3.Vec3, yOffset: number): v3.Vec3 {
    const terrain = this.objects.terrain as TextureTerrain;

    const trim = (x: number): number => {
      const offset = 0.5;
      return Math.max(-this.terrainScale + offset, Math.min(x, this.terrainScale - offset));
    };

    const neighbours = 2;
    let heightsSum = 0;

    for (let i = -neighbours; i <= neighbours; i += 1) {
      for (let j = -neighbours; j <= neighbours; j += 1) {
        const x = trim(pos[0] + i);
        const z = trim(pos[2] + j);

        heightsSum += terrain.getZ(
          this.mapLocalToTerrain(x), this.mapLocalToTerrain(-z),
        ) * this.terrainScale * this.terrainYScaleFactor;
      }
    }

    const y = heightsSum / ((2 * neighbours + 1) ** 2);

    return [trim(pos[0]), y + yOffset, trim(pos[2])];
  }

  protected async setupObjects(): Promise<void> {
    this.terrainScale = 30;
    await super.setupObjects();

    await this.addObject('crosshair', new Crosshair());
    await this.addObject('cube1', new Cube());
    await this.addObject('cube2', new Cube());
    await this.addObject('cube3', new Cube());
    await this.addObject('sponge', new Sponge());
    await this.addObject('penguin', new Penguin());
    await this.addObject('andy', new Andy());

    const camera = this.camera as FirstPersonCamera;
    camera.setPosition(-3, 4);
    camera.yaw(0.3);

    this.setupCallbacks();

    this.objects.cube1.transform.translate([5, 3, 5]);
    this.objects.cube2.transform.translate([-5, 5, 5]);
    this.objects.cube3.transform.translate([-5, 7, -5]);

    this.objects.sponge.transform.position = this.positionConstraint(
      [-2, 0, 0], -this.objects.sponge.bounds.yMin,
    );
    this.objects.penguin.transform.position = this.positionConstraint(
      [-3, 0, -10], -this.objects.penguin.bounds.yMin,
    );
    this.objects.andy.transform.position = this.positionConstraint(
      [20, 0, -20], -this.objects.penguin.bounds.yMin,
    );
  }

  protected setupMovementCallbacks(): void {
    document.addEventListener('keydown', (event) => { this.pressedKeys[event.keyCode] = true; });
    document.addEventListener('keyup', (event) => { this.pressedKeys[event.keyCode] = false; });
    document.addEventListener('keypress', (event) => this.onKeyPress(event));

    document.addEventListener('pointerlockchange', () => this.onLockChange(), false);
    document.addEventListener('mozpointerlockchange', () => this.onLockChange(), false);
    document.addEventListener('mousemove', (e) => this.rotateFirstPersonCamera(e), false);

    const canvas = this.gl.canvas as HTMLCanvasElement;
    canvas.onclick = (): void => this.requestLock();
  }

  protected setupInspectionCallbacks(): void {
    document.addEventListener('mousedown', (event) => this.beginDrag(event));
    document.addEventListener('mouseup', () => this.endDrag());
    document.addEventListener('mousemove', (event) => this.onDrag(event));
  }

  protected setupCallbacks(): void {
    this.setupMovementCallbacks();
    this.setupInspectionCallbacks();
  }

  protected onStopRender(): void {
    this.stopInspect();
  }

  requestLock(): void {
    const canvas = this.gl.canvas as HTMLCanvasElement;
    if (this.isRunning && !this.inspectedObject) {
      canvas.requestPointerLock();
    }
  }

  private onKeyPress(e: KeyboardEvent): void {
    if (e.keyCode === this.keyCodes.e) {
      if (this.inspectedObject) {
        this.stopInspect();
        this.requestLock();
        this.resetCameraHelp();
      } else if (this.currentObject && this.currentObject.canBeInspected) {
        this.beginInspect(this.currentObject);
        document.exitPointerLock();

        this.cameraHelp.textContent = 'Press and drag your mouse to rotate selected object. Press E again to exit inspection';
      }
    }
  }

  private onLockChange(): void {
    if (this.isRunning && document.pointerLockElement === this.gl.canvas) {
      this.mouseLocked = true;
    } else {
      this.mouseLocked = false;
    }
  }

  private rotateFirstPersonCamera(e: MouseEvent): void {
    if (this.mouseLocked) {
      const camera = this.camera as FirstPersonCamera;
      camera.yaw(e.movementX * this.rotationSpeed);
      camera.pitch(e.movementY * this.rotationSpeed);
    }
  }

  protected moveCamera(): void {
    const camera = this.camera as FirstPersonCamera;

    if (!this.inspectedObject) {
      if (this.pressedKeys[this.keyCodes.forward]) camera.move(this.getMovementSpeed());
      if (this.pressedKeys[this.keyCodes.backward]) camera.move(-this.getMovementSpeed());
      if (this.pressedKeys[this.keyCodes.right]) camera.strafe(this.getMovementSpeed());
      if (this.pressedKeys[this.keyCodes.left]) camera.strafe(-this.getMovementSpeed());
    }
  }

  protected beginDrag(event: MouseEvent): void {
    this.drag.startX = event.clientX;
    this.drag.startY = event.clientY;

    this.drag.isDragging = true;
  }

  protected endDrag(): void {
    this.drag.isDragging = false;
    if (this.inspectedObject && this.inspectedObject.orbitalCamera) {
      this.inspectedObject.orbitalCamera.commitRotation();
    }
  }

  protected onDrag(event: MouseEvent): void {
    if (this.inspectedObject && this.inspectedObject.orbitalCamera && this.drag.isDragging) {
      const dx = event.clientX - this.drag.startX;
      const dy = event.clientY - this.drag.startY;

      this.inspectedObject.orbitalCamera.rotateXY(-dx, dy);
    }
  }

  private resetCameraHelp(): void {
    this.cameraHelp.textContent = 'Press E to inspect objects';
  }

  protected createOverlay(): void {
    super.createOverlay();
    if (this.overlay) {
      this.overlay.style.width = '275px';

      const text = document.createElement('div');
      text.textContent = 'Use WSAD to move around. Click on scene to enable mouse movement.';
      text.style.marginBottom = '10px';
      this.overlay.appendChild(text);

      this.cameraHelp = document.createElement('div');
      this.resetCameraHelp();
      this.overlay.appendChild(this.cameraHelp);
    }
  }
}
