import { fabric } from 'fabric';
import { Scene } from '../../base/scene';
import { RandomColorTerrain } from '../../objects/random_color_terrain';
import { degToRad } from '../../utils';
import { Camera } from '../../base/camera';
import { FloatingCamera } from '../../cameras/floating_camera';

export class InteractiveTerrainScene extends Scene {
  private ctx!: CanvasRenderingContext2D;

  private imageData!: ImageData;

  private reinitialize = false;

  private meshSize = 128;

  protected createCamera(): Camera {
    return new FloatingCamera();
  }

  protected createOverlay(): void {
    super.createOverlay();
    const overlay = this.overlay as HTMLDivElement;
    overlay.style.width = '257px';

    const text = document.createElement('div');
    text.textContent = 'Draw with your mouse in canvas below to create some terrain (ctrl + R to reset)';
    text.style.marginBottom = '10px';
    overlay.appendChild(text);

    const canvas = document.createElement('canvas');
    canvas.width = 256;
    canvas.height = 256;
    canvas.style.border = '1px solid black';
    overlay.appendChild(canvas);
    this.ctx = canvas.getContext('2d') as CanvasRenderingContext2D;

    const fabricCanvas = new fabric.Canvas(canvas);
    fabricCanvas.setBackgroundColor('rgba(0, 0, 0, 1)', fabricCanvas.renderAll.bind(fabricCanvas));
    fabricCanvas.isDrawingMode = true;
    fabricCanvas.freeDrawingBrush.width = 20;
    fabricCanvas.freeDrawingBrush.color = '#ffffff1f';
    fabricCanvas.on('path:created', (): void => { this.onUpdate(); });
  }

  private async onUpdate(): Promise<void> {
    this.updateImageData();
    this.reinitialize = true;
  }

  private updateImageData(): void {
    if (this.ctx) {
      this.imageData = this.ctx.getImageData(0, 0, this.ctx.canvas.width, this.ctx.canvas.height);
    }
  }

  protected getZFromCanvas(x: number, y: number): number {
    if (this.ctx && this.imageData) {
      const scaleY = this.ctx.canvas.height / this.meshSize;
      const scaleX = this.ctx.canvas.width / this.meshSize;

      const canvasX = Math.round(x * scaleX);
      const canvasY = this.ctx.canvas.height - Math.round(y * scaleY) - 1;

      return this.imageData.data[4 * canvasY * this.ctx.canvas.width + 4 * canvasX] / 255;
    }
    return 0;
  }

  protected async setupObjects(): Promise<void> {
    const camera = this.camera as FloatingCamera;
    camera.reset();
    camera.move([0, 0, 5]);

    this.updateImageData();
    await this.addObject('landscape', new RandomColorTerrain(
      this.meshSize, (x, y): number => this.getZFromCanvas(x, y),
    ));
  }

  protected placeObjects(msec: number): void {
    if (this.reinitialize) {
      (this.objects.landscape as RandomColorTerrain).recreatePositions();
      this.reinitialize = false;
    }

    this.objects.landscape.transform.reset();
    this.objects.landscape.transform.rotateX(degToRad(-60));
    this.objects.landscape.transform.rotateZ(msec * 0.001 * 0.3);
    // this.objects.landscape.transform.scale([3, 3, 1]);
  }
}
