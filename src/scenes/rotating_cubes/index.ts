import { Scene } from '../../base/scene';
import { Cube } from '../../objects/cube';
import { Camera } from '../../base/camera';
import { FloatingCamera } from '../../cameras/floating_camera';

export class RotatingCubesScene extends Scene {
  protected createCamera(): Camera {
    return new FloatingCamera();
  }

  async setupObjects(): Promise<void> {
    const camera = this.camera as FloatingCamera;
    camera.reset();
    camera.move([0, 0, 10]);

    await Promise.all([
      this.addObject('cube1', new Cube()),
      this.addObject('cube2', new Cube()),
    ]);
  }

  placeObjects(msec: number): void {
    const time = msec * 0.001;
    const radius = 2;
    this.objects.cube1.transform.reset();

    this.objects.cube1.transform.translate([
      Math.cos(time) * radius,
      0,
      Math.sin(time) * radius,
    ]);
    this.objects.cube1.transform.rotate(time + 5, [1, 0, 1]);

    this.objects.cube2.transform.reset();
    this.objects.cube2.transform.translate([
      -Math.cos(time) * radius,
      0,
      -Math.sin(time) * radius,
    ]);
    this.objects.cube2.transform.rotate(time, [1, 0, 1]);
  }
}
