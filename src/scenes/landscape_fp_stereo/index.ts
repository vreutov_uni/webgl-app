import { StereoScene } from '../stereo_scene';
import { FirstPersonLandscapeScene } from '../landscape_fp';

export class FirstPersonLandscapeStereoScene extends StereoScene<FirstPersonLandscapeScene> {
  constructor(
    leftCanvas: HTMLCanvasElement,
    rightCanvas: HTMLCanvasElement,
    gl: WebGLRenderingContext, overlay?: HTMLDivElement,
  ) {
    super(FirstPersonLandscapeScene, leftCanvas, rightCanvas, gl, overlay);

    document.addEventListener('pointerlockchange', (ev) => this.onLockChange(ev), false);
    document.addEventListener('mozpointerlockchange', (ev) => this.onLockChange(ev), false);

    const canvas = this.gl.canvas as HTMLCanvasElement;
    canvas.onclick = (): void => this.requestLock();

    this.leftScene.requestLock = (): void => { this.requestLock(); };
    this.rightScene.requestLock = (): void => {};
  }

  private onLockChange(ev: Event): void {
    if (this.isRunning && document.pointerLockElement === this.gl.canvas) {
      this.leftScene.mouseLocked = true;
      this.rightScene.mouseLocked = true;

      ev.stopImmediatePropagation();
    } else {
      this.leftScene.mouseLocked = false;
      this.rightScene.mouseLocked = false;
    }
  }

  private requestLock(): void {
    if (this.isRunning && !this.leftScene.inspectedObject) {
      const canvas = this.gl.canvas as HTMLCanvasElement;
      canvas.requestPointerLock();
    }
  }

  protected createOverlay(): void {
    super.createOverlay();
    if (this.overlay) {
      this.overlay.style.width = '200px';

      const eyeLabel = document.createElement('div');
      eyeLabel.innerText = `Eye separation: ${this.eyeSeparation}`;
      this.overlay.appendChild(eyeLabel);

      const eyeInput = document.createElement('input');
      eyeInput.type = 'range';
      eyeInput.min = '0';
      eyeInput.max = '0.5';
      eyeInput.step = '0.01';
      eyeInput.value = this.eyeSeparation.toString();
      eyeInput.style.width = '100%';
      this.overlay.appendChild(eyeInput);

      eyeInput.addEventListener('input', () => {
        this.eyeSeparation = Number.parseFloat(eyeInput.value);
        eyeLabel.innerText = `Eye separation: ${this.eyeSeparation}`;
      });

      const convergenceLabel = document.createElement('div');
      convergenceLabel.innerText = `Convergence distance: ${this.convergenceDistance}`;
      this.overlay.appendChild(convergenceLabel);

      const { zNear, zFar } = this.projectionOptions;
      const convergenceInput = document.createElement('input');
      convergenceInput.type = 'range';
      convergenceInput.min = zNear.toString();
      convergenceInput.max = Math.min(zFar, 20).toString();
      convergenceInput.step = '0.5';
      convergenceInput.value = this.convergenceDistance.toString();
      convergenceInput.style.width = '100%';
      this.overlay.appendChild(convergenceInput);

      convergenceInput.addEventListener('input', () => {
        this.convergenceDistance = Number.parseFloat(convergenceInput.value);
        convergenceLabel.innerText = `Convergence distance: ${this.convergenceDistance}`;
      });
    }
  }
}
