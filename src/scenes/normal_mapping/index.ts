import { m4 } from 'twgl.js';
import { Scene } from '../../base/scene';
import { Camera } from '../../base/camera';
import { Brickwall } from '../../objects/brickwall';
import { FloatingCamera } from '../../cameras/floating_camera';
import { degToRad } from '../../utils';

export class NormalMappingScene extends Scene {
  private drag = {
    isDragging: false,
    startX: 0,
    startY: 0,
    startRotation: m4.identity(),
  }

  private pressedKeys: {
    [key: number]: boolean;
  } = {}

  private keyCodes = {
    shift: 16,
  }

  protected createCamera(): Camera {
    return new FloatingCamera();
  }

  protected placeObjects(): void {
    // dummy
  }

  protected async setupObjects(): Promise<void> {
    await this.addObject('wall', new Brickwall());
    this.objects.wall.transform.translate([0, 0, -4]);
    this.objects.wall.transform.rotateY(degToRad(-45));

    this.lightPosition = [-2, 2, 0];

    document.addEventListener('mousedown', (event) => this.beginDrag(event));
    document.addEventListener('mouseup', () => this.endDrag());
    document.addEventListener('mousemove', (event) => this.onDrag(event));
  }

  protected beginDrag(event: MouseEvent): void {
    this.drag.startX = event.clientX;
    this.drag.startY = event.clientY;
    m4.copy(this.objects.wall.transform.rotation, this.drag.startRotation);

    this.drag.isDragging = true;
  }

  protected endDrag(): void {
    this.drag.isDragging = false;
  }

  protected onDrag(event: MouseEvent): void {
    if (this.drag.isDragging) {
      const dx = event.clientX - this.drag.startX;
      const dy = event.clientY - this.drag.startY;
      const rotSpeed = Math.PI / Math.min(this.gl.canvas.width, this.gl.canvas.height);

      const rotation = m4.identity();
      m4.copy(this.drag.startRotation, rotation);

      m4.axisRotate(
        rotation,
        [rotation[1], rotation[5], rotation[9]],
        dx * rotSpeed, rotation,
      );
      m4.axisRotate(
        rotation,
        [rotation[0], rotation[4], rotation[8]],
        dy * rotSpeed, rotation,
      );

      this.objects.wall.transform.rotation = rotation;
    }
  }
}
