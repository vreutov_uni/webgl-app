import { Scene } from '../../base/scene';
import { degToRad } from '../../utils';
// import TerrainTexture from './textures/blue.png';
import TerrainTexture from './textures/weeds.png';
// import TerrainImage from './terrains/circles.png';
import TerrainImage from './terrains/mountains.png';
import { TextureTerrain } from '../../objects/texture_terrain';

import SkyboxUp from './textures/ame_fade/up.png';
import SkyboxDown from './textures/ame_fade/down.png';
import SkyboxFront from './textures/ame_fade/front.png';
import SkyboxBack from './textures/ame_fade/back.png';
import SkyboxLeft from './textures/ame_fade/left.png';
import SkyboxRight from './textures/ame_fade/right.png';
import { Skybox } from '../../objects/skybox';
import { Camera } from '../../base/camera';
import { FloatingCamera } from '../../cameras/floating_camera';

export class LandscapeScene extends Scene {
  protected meshSize = 256;

  protected terrainScale = 15;

  protected terrainYScaleFactor = 0.6;

  protected createCamera(): Camera {
    return new FloatingCamera();
  }

  protected async setupObjects(): Promise<void> {
    await this.addObject('skybox', new Skybox(
      {
        posx: SkyboxFront,
        negx: SkyboxBack,
        posy: SkyboxUp,
        negy: SkyboxDown,
        posz: SkyboxRight,
        negz: SkyboxLeft,
      },
    ));

    await this.addObject('terrain', new TextureTerrain(
      this.meshSize, TerrainImage,
      TerrainTexture, 64,
    ));
  }

  protected moveCamera(msec: number): void {
    const time = msec * 0.001;
    const cameraRadius = 14;
    const camera = this.camera as FloatingCamera;

    camera.reset();
    camera.setToTarget([0, 0, 0]);
    camera.move([
      Math.cos(time * 0.2) * cameraRadius,
      5,
      Math.sin(time * 0.2) * cameraRadius,
    ]);
  }

  protected placeObjects(msec: number): void {
    this.moveCamera(msec);

    const { terrain, skybox } = this.objects;
    const scale = this.terrainScale;

    terrain.transform.reset();
    terrain.transform.scale([scale, scale, scale * this.terrainYScaleFactor]);
    terrain.transform.rotateX(degToRad(-90));

    skybox.transform.reset();
    skybox.transform.scale([scale, scale, scale]);

    this.lightPosition = [-scale, scale * this.terrainYScaleFactor, 0];
  }
}
