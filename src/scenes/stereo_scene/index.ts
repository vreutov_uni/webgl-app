/* eslint-disable @typescript-eslint/no-this-alias */
/* eslint-disable max-classes-per-file */

import { m4, v3 } from 'twgl.js';
import { Scene } from '../../base/scene';
import { Camera } from '../../base/camera';
import { createGLContextFromCanvas } from '../../utils';
import { FloatingCamera } from '../../cameras/floating_camera';
import { StereoScreen } from './screen';

type SceneConstructor<T extends Scene> = new (
  gl: WebGLRenderingContext, overlay?: HTMLDivElement
) => T;

export class StereoScene<T extends Scene> extends Scene {
  protected leftScene: T;

  protected rightScene: T;

  public eyeSeparation: number;

  public convergenceDistance: number;

  constructor(
    SceneClass: SceneConstructor<T>,
    leftCanvas: HTMLCanvasElement, rightCanvas: HTMLCanvasElement,
    gl: WebGLRenderingContext, overlay?: HTMLDivElement,
  ) {
    super(gl, overlay);

    const self = this;
    this.eyeSeparation = 0.1;
    this.convergenceDistance = 5;

    abstract class LeftScene extends (SceneClass as SceneConstructor<Scene>) {
      protected isLeft = true;

      protected getView(): m4.Mat4 {
        let dist = self.eyeSeparation / 2;
        if (this.isLeft) dist *= -1;

        const camera = this.camera.toMatrix();
        const xaxis = m4.getAxis(camera, 0) as unknown as v3.Vec3;
        xaxis[0] *= dist;
        xaxis[2] *= dist;

        m4.translate(camera, xaxis, camera);
        return m4.inverse(camera);
      }

      protected getProjection(): m4.Mat4 {
        const canvas = this.gl.canvas as HTMLCanvasElement;
        const near = this.projectionOptions.zNear;
        const far = this.projectionOptions.zFar;
        const { fov } = this.projectionOptions;

        const aspect = (canvas.clientWidth) / (canvas.clientHeight);
        const tfov2 = Math.tan(fov / 2);
        const top = near * tfov2;
        const dist = self.eyeSeparation / 2;
        const a = aspect * tfov2 * self.convergenceDistance;
        const b = a - dist;
        const c = a + dist;
        const s = near / self.convergenceDistance;

        if (this.isLeft) {
          return m4.frustum(-b * s, c * s, -top, top, near, far);
        }
        return m4.frustum(-c * s, b * s, -top, top, near, far);
      }
    }

    abstract class RightScene extends LeftScene {
      protected isLeft = false;
    }

    this.leftScene = new (
      LeftScene as unknown as SceneConstructor<T>
    )(createGLContextFromCanvas(leftCanvas));

    this.rightScene = new (
      RightScene as unknown as SceneConstructor<T>
    )(createGLContextFromCanvas(rightCanvas));
  }

  protected onBeginRender(): void {
    super.onBeginRender();
    this.leftScene.beginRenderLoop();
    this.rightScene.beginRenderLoop();
  }

  protected onStopRender(): void {
    super.onStopRender();
    this.leftScene.stopRenderLoop();
    this.rightScene.stopRenderLoop();
  }

  protected createCamera(): Camera {
    return new FloatingCamera();
  }

  protected async setupObjects(): Promise<void> {
    await this.addObject('screen', new StereoScreen(
      this.leftScene.gl.canvas as HTMLCanvasElement,
      this.rightScene.gl.canvas as HTMLCanvasElement,
    ));

    this.objects.screen.transform.translate([0, 0, -2]);
  }

  protected placeObjects(): void {
    (this.objects.screen as StereoScreen).updateTextures();
  }
}
