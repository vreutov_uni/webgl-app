
import {
  BufferInfo,
  createBufferInfoFromArrays,
  createTexture,
  setTextureFromElement,
} from 'twgl.js';
import { GLObject, Uniforms } from '../../../base/object';
import vsSource from './shaders/screen.vert';
import fsSource from './shaders/screen.frag';

export class StereoScreen extends GLObject {
  private leftSceneCanvas: HTMLCanvasElement;

  private rightSceneCanvas: HTMLCanvasElement;

  constructor(leftSceneCanvas: HTMLCanvasElement, rightSceneCanvas: HTMLCanvasElement) {
    super();
    this.leftSceneCanvas = leftSceneCanvas;
    this.rightSceneCanvas = rightSceneCanvas;
  }

  getVS(): string {
    return vsSource;
  }

  getFS(): string {
    return fsSource;
  }

  getInspectingFS(): string {
    return this.getFS();
  }

  createUniforms(): Uniforms {
    return {
      uMapL: createTexture(this.gl, { src: this.leftSceneCanvas }),
      uMapR: createTexture(this.gl, { src: this.rightSceneCanvas }),
    };
  }

  updateTextures(): void {
    setTextureFromElement(this.gl, this.uniforms.uMapL, this.leftSceneCanvas);
    setTextureFromElement(this.gl, this.uniforms.uMapR, this.rightSceneCanvas);
  }

  async createBufferInfo(): Promise<BufferInfo> {
    const verticles = [
      [-1.0, 1.0, 0.0],
      [-1.0, -1.0, 0.0],
      [1.0, -1.0, 0.0],
      [1.0, 1.0, 0.0],
    ];

    const texcoords = [
      [0.0, 0.0],
      [0.0, 1.0],
      [1.0, 1.0],
      [1.0, 0.0],
    ];

    const indices = [
      0, 1, 2, 0, 2, 3,
    ];

    const arrays = {
      aPosition: {
        numComponents: 3,
        data: ([] as number[]).concat(...verticles),
      },
      aTexcoord: {
        numComponents: 2,
        data: ([] as number[]).concat(...texcoords),
      },
      indices: {
        numComponents: 3,
        data: indices,
      },
    };
    return createBufferInfoFromArrays(this.gl, arrays);
  }
}
