attribute vec4 aPosition;
attribute vec2 aTexcoord;

uniform mat4 uModelViewProjection;

varying vec2 vTexcoord;

void main() {
    vTexcoord = aTexcoord;
    gl_Position = uModelViewProjection * aPosition;
}
