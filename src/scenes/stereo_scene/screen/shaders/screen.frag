precision mediump float;

uniform sampler2D uMapL;
uniform sampler2D uMapR;

varying vec2 vTexcoord;

void main()
{
  const mat3 mL = mat3(0.4155, -0.0458, -0.0545, 0.4710, -0.0484, -0.0614, 0.1670, -0.0258, 0.0128);
  const mat3 mR = mat3(-0.0109, 0.3756, -0.0651, -0.0365, 0.7333, -0.1286, -0.0060, 0.0111, 1.2968);
  vec3 colorL = texture2D(uMapL, vTexcoord).rgb;
  vec3 colorR = texture2D(uMapR, vTexcoord).rgb;

  gl_FragColor = vec4(mL * colorL + mR * colorR, 1.0);
}
