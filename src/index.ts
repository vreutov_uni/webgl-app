/* eslint-disable @typescript-eslint/no-unused-vars */

import { connect } from 'socket.io-client';
import { createGLContext } from './utils';
import { Scene } from './base/scene';
import { RotatingCubesScene } from './scenes/rotating_cubes';
import { InteractiveTerrainScene } from './scenes/interactive_terrain';
import { LandscapeScene } from './scenes/landscape';
import { FirstPersonLandscapeScene } from './scenes/landscape_fp';
import { NormalMappingScene } from './scenes/normal_mapping';
import { StereoScene } from './scenes/stereo_scene';
import { FirstPersonLandscapeWithSocketScene } from './scenes/landscape_fp_socket';
import { FirstPersonLandscapeStereoScene } from './scenes/landscape_fp_stereo';


window.onload = (): void => {
  const gl = createGLContext('glCanvas');

  const canvasLeft = document.createElement('canvas');
  canvasLeft.height = 1024;
  canvasLeft.width = 1024;
  document.body.appendChild(canvasLeft);

  const canvasRight = document.createElement('canvas');
  canvasRight.height = 1024;
  canvasRight.width = 1024;
  document.body.appendChild(canvasRight);

  const sceneOverlay = document.getElementById('glCanvasOverlay') as HTMLDivElement;
  const ioClient = connect();

  const scenes = new Map<string, Scene>();
  scenes.set('Cubes', new RotatingCubesScene(gl, sceneOverlay));
  scenes.set('Interactive terrain', new InteractiveTerrainScene(gl, sceneOverlay));
  scenes.set('Landscape', new LandscapeScene(gl, sceneOverlay));
  scenes.set('First person landscape', new FirstPersonLandscapeScene(gl, sceneOverlay));
  scenes.set('Normal mapping', new NormalMappingScene(gl, sceneOverlay));
  scenes.set('Socket rotation', new FirstPersonLandscapeWithSocketScene(ioClient, gl, sceneOverlay));

  scenes.set('Stereo landscape', new FirstPersonLandscapeStereoScene(canvasLeft, canvasRight, gl, sceneOverlay));

  let currentScene = scenes.get('Stereo landscape') as Scene;

  const select = document.getElementById('scenes') as HTMLSelectElement;
  scenes.forEach((value, key): void => {
    const option = document.createElement('option');
    option.text = key;
    option.selected = value === currentScene;
    select.appendChild(option);
  });

  select.onchange = (): void => {
    currentScene.stopRenderLoop();
    currentScene = scenes.get(select.selectedOptions[0].text) as Scene;
    currentScene.beginRenderLoop();
  };

  currentScene.beginRenderLoop();
};
