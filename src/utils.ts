import { m4 } from 'twgl.js';

export function createGLContextFromCanvas(canvas: HTMLCanvasElement): WebGLRenderingContext {
  const gl = canvas.getContext('webgl');

  if (!gl) {
    throw new Error('Unable to initialize WebGL. Your browser or machine may not support it.');
  }

  return gl;
}

export function createGLContext(canvasId: string): WebGLRenderingContext {
  const canvas = document.getElementById(canvasId) as HTMLCanvasElement;
  return createGLContextFromCanvas(canvas);
}

export function createRandomColor(opaque = 1.0): number[] {
  return [Math.random(), Math.random(), Math.random(), opaque];
}

export function degToRad(degree: number): number {
  return (degree * Math.PI) / 180;
}

export class ColorEncoder {
  private scale = {
    red: 0,
    green: 0,
    blue: 0,
    alpha: 0,
  }

  private shift = {
    red: 0,
    green: 0,
    blue: 0,
  }

  constructor(gl: WebGLRenderingContext) {
    const bits = {
      red: gl.getParameter(gl.RED_BITS),
      green: gl.getParameter(gl.GREEN_BITS),
      blue: gl.getParameter(gl.BLUE_BITS),
      alpha: gl.getParameter(gl.ALPHA_BITS),
    };

    this.scale.red = 2 ** bits.red;
    this.scale.green = 2 ** bits.green;
    this.scale.blue = 2 ** bits.blue;
    this.scale.alpha = 2 ** bits.alpha;

    this.shift.red = 2 ** (bits.green + bits.blue + bits.alpha);
    this.shift.green = 2 ** (bits.blue + bits.alpha);
    this.shift.blue = 2 ** (bits.alpha);
  }

  getId(color: Uint8Array): number {
    return (
      color[0] * this.shift.red
      + color[1] * this.shift.green
      + color[2] * this.shift.blue
      + color[3]
    );
  }

  getColor(id: number): number[] {
    let x = id;

    const r = Math.floor(x / this.shift.red);
    x -= (r * this.shift.red);

    const g = Math.floor(x / this.shift.green);
    x -= (g * this.shift.green);

    const b = Math.floor(x / this.shift.blue);
    x -= (b * this.shift.blue);

    const a = x;

    return [
      r / (this.scale.red - 1),
      g / (this.scale.green - 1),
      b / (this.scale.blue - 1),
      a / (this.scale.alpha - 1),
    ];
  }
}

export type Bounds = {
  xMin: number;
  xMax: number;
  yMin: number;
  yMax: number;
  zMin: number;
  zMax: number;
}

export function computeBounds(verticles: number[]): Bounds {
  let [xMin, yMin, zMin, xMax, yMax, zMax] = verticles;

  for (let i = 0; i < verticles.length; i += 3) {
    xMin = Math.min(xMin, verticles[i]);
    xMax = Math.max(xMax, verticles[i]);

    yMin = Math.min(yMin, verticles[i + 1]);
    yMax = Math.max(yMax, verticles[i + 1]);

    zMin = Math.min(zMin, verticles[i + 2]);
    zMax = Math.max(zMax, verticles[i + 2]);
  }

  return {
    xMin,
    xMax,
    yMin,
    yMax,
    zMin,
    zMax,
  };
}

export function verticlesToNDC(verticles: number[]): void {
  const {
    xMin, yMin, zMin, xMax, yMax, zMax,
  } = computeBounds(verticles);

  const offsetX = (xMax - xMin) / 2;
  const offsetY = (yMax - yMin) / 2;
  const offsetZ = (zMax - zMin) / 2;

  const maxOffset = Math.max(offsetX, offsetY, offsetZ);

  for (let i = 0; i < verticles.length; i += 3) {
    verticles[i] = (verticles[i] - xMin - offsetX) / maxOffset;
    verticles[i + 1] = (verticles[i + 1] - yMin - offsetY) / maxOffset;
    verticles[i + 2] = (verticles[i + 2] - zMin - offsetZ) / maxOffset;
  }
}

export function quaternionToRotationMatrix(quaternion: number[]): m4.Mat4 {
  const mat = m4.identity();
  const [qw, qx, qy, qz] = quaternion;
  mat[0] = 1 - 2 * qy ** 2 - 2 * qz ** 2;
  mat[1] = 2 * qx * qy - 2 * qz * qw;
  mat[2] = 2 * qx * qz + 2 * qy * qw;

  mat[4] = 2 * qx * qy + 2 * qz * qw;
  mat[5] = 1 - 2 * qx ** 2 - 2 * qz ** 2;
  mat[6] = 2 * qy * qz - 2 * qx * qw;

  mat[8] = 2 * qx * qz - 2 * qy * qw;
  mat[9] = 2 * qy * qz + 2 * qx * qw;
  mat[10] = 1 - 2 * qx ** 2 - 2 * qy ** 2;

  return mat;
}
