import { v3, m4 } from 'twgl.js';
import { Camera } from '../base/camera';

export class FloatingCamera implements Camera {
  protected position!: v3.Vec3;

  protected direction!: v3.Vec3;

  protected target?: v3.Vec3;

  protected up!: v3.Vec3;

  constructor() {
    this.reset();
  }

  reset(): void {
    this.position = v3.create();
    this.direction = [0, 0, -1];
    this.up = [0, 1, 0];
    this.unsetTarget();
  }

  toMatrix(): m4.Mat4 {
    const target = v3.create();
    if (this.target) {
      v3.copy(this.target, target);
    } else {
      v3.add(this.position, this.direction, target);
    }

    return m4.lookAt(this.position, target, this.up);
  }

  move(vector: v3.Vec3): void {
    v3.add(this.position, vector, this.position);
  }

  setToTarget(target: v3.Vec3): void {
    this.target = target;
  }

  unsetTarget(): void {
    this.target = undefined;
  }
}
