import { v3, m4 } from 'twgl.js';
import { Camera } from '../base/camera';

export type CameraOrientation = {
  yaw: number;
  pitch: number;
  roll: number;
}

export type PositionConstraintCallback = (pos: v3.Vec3, yOffset: number) => v3.Vec3;

export class FirstPersonCamera implements Camera {
  orientation: CameraOrientation;

  position: v3.Vec3;

  protected heightOffset: number;

  protected positionConstraint: PositionConstraintCallback;

  constructor(heightOffset: number, positionConstraint: PositionConstraintCallback) {
    this.heightOffset = heightOffset;
    this.position = [0, 0, 0];
    this.orientation = {
      yaw: 0,
      pitch: 0,
      roll: 0,
    };
    this.positionConstraint = positionConstraint;
  }

  toMatrix(): m4.Mat4 {
    const mat = m4.identity();
    m4.translate(mat, this.position, mat);
    m4.rotateZ(mat, this.orientation.roll, mat);
    m4.rotateY(mat, this.orientation.yaw, mat);
    m4.rotateX(mat, this.orientation.pitch, mat);

    return mat;
  }

  setPosition(x: number, z: number): void {
    this.position = this.positionConstraint([x, 0, z], this.heightOffset);
  }

  private moveToAngle(angle: number, delta: number): void {
    this.position[0] -= Math.sin(angle) * delta;
    this.position[2] -= Math.cos(angle) * delta;

    this.position = this.positionConstraint(this.position, this.heightOffset);
  }

  move(delta: number): void {
    this.moveToAngle(this.orientation.yaw, delta);
  }

  strafe(delta: number): void {
    const a = this.orientation.yaw - Math.PI / 2.0;
    this.moveToAngle(a, delta);
  }

  private wrap(value: number, min: number, max: number): number {
    const normValue = value - min;
    const normMax = max - min;

    if (max === 0) return min;
    const a = (normValue % normMax) + min;
    if (a < min) return a + normMax;
    return a;
  }

  private crop(value: number, min: number, max: number): number {
    if (value > max) return max;
    if (value < min) return min;
    return value;
  }

  yaw(angle: number): void {
    this.orientation.yaw = this.wrap(
      this.orientation.yaw - angle,
      0.0, 2 * Math.PI,
    );
  }

  pitch(angle: number): void {
    this.orientation.pitch = this.crop(
      this.orientation.pitch - angle,
      -Math.PI / 2, Math.PI / 2,
    );
  }
}
