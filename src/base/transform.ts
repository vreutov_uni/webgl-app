import { m4, v3 } from 'twgl.js';

export class Transform {
  position!: v3.Vec3;

  rotation!: m4.Mat4;

  scaling!: v3.Vec3;

  constructor() {
    this.reset();
  }

  toMatrix(): m4.Mat4 {
    const mat = m4.identity();
    m4.translate(mat, this.position, mat);
    m4.multiply(mat, this.rotation, mat);
    m4.scale(mat, this.scaling, mat);
    return mat;
  }

  reset(): void {
    this.position = v3.create();
    this.rotation = m4.identity();
    this.scaling = v3.create(1, 1, 1);
  }

  translate(vector: v3.Vec3): void {
    v3.add(this.position, vector, this.position);
  }

  rotate(angle: number, axis: v3.Vec3): void {
    m4.axisRotate(this.rotation, axis, angle, this.rotation);
  }

  rotateX(angle: number): void {
    this.rotate(angle, [1, 0, 0]);
  }

  rotateY(angle: number): void {
    this.rotate(angle, [0, 1, 0]);
  }

  rotateZ(angle: number): void {
    this.rotate(angle, [0, 0, 1]);
  }

  scale(vector: v3.Vec3): void {
    v3.multiply(this.scaling, vector, this.scaling);
  }
}
