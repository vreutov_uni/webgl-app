export abstract class GLElement {
  gl!: WebGLRenderingContext;

  initialize(gl: WebGLRenderingContext): void {
    this.gl = gl;
  }
}
