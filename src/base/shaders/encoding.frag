precision mediump float;

uniform vec4 uEncodingColor;

void main() {
    gl_FragColor = uEncodingColor;
}
