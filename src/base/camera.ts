import { m4, v3 } from 'twgl.js';
import { GLObject } from './object';
import { quaternionToRotationMatrix } from '../utils';

export interface Camera {
  toMatrix(): m4.Mat4;
}

export class OribitalCamera implements Camera {
  protected object: GLObject;

  protected rotation: m4.Mat4;

  protected currentRotation: m4.Mat4;

  protected right: v3.Vec3;

  rotationSpeed: number;

  offset: number;

  constructor(object: GLObject) {
    this.object = object;
    this.rotation = m4.identity();
    this.currentRotation = m4.identity();
    this.rotationSpeed = Math.PI / Math.min(object.gl.canvas.width, object.gl.canvas.height);
    this.offset = 5;
    this.right = [-1, 0, 0];
  }

  toMatrix(): m4.Mat4 {
    const camera = m4.identity();
    m4.translate(camera, this.object.transform.position, camera);
    m4.multiply(camera, this.currentRotation, camera);
    m4.translate(camera, [0, 0, -this.offset], camera);

    const cameraUp = m4.identity();
    m4.translate(cameraUp, this.object.transform.position, cameraUp);
    m4.multiply(cameraUp, this.currentRotation, cameraUp);
    m4.translate(cameraUp, [0, 1, -this.offset], cameraUp);

    const position = [camera[12], camera[13], camera[14]];
    const positionUp = [cameraUp[12], cameraUp[13], cameraUp[14]];

    const up = v3.subtract(positionUp, position);
    return m4.lookAt(position, this.object.transform.position, up);
  }

  rotateXY(dx: number, dy: number): void {
    const r = m4.identity();
    m4.copy(this.rotation, r);

    m4.rotateY(r, dx * this.rotationSpeed, r);
    m4.rotateX(r, dy * this.rotationSpeed, r);

    this.normalize(r);
    m4.copy(r, this.currentRotation);
  }

  rotateWithQuaternion(quaternion: number[]): void {
    this.currentRotation = quaternionToRotationMatrix(quaternion);
  }

  commitRotation(): void {
    m4.copy(this.currentRotation, this.rotation);
  }

  private normalize(mat: m4.Mat4): void {
    const normalizeItems = (i: number, j: number, k: number): void => {
      const n = [mat[i], mat[j], mat[k]];
      v3.normalize(n, n);

      // eslint-disable-next-line no-param-reassign
      [mat[i], mat[j], mat[k]] = n;
    };

    normalizeItems(0, 4, 8);
    normalizeItems(1, 5, 9);
    normalizeItems(2, 6, 10);
  }
}
