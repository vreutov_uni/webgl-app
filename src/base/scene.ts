import {
  m4,
  setBuffersAndAttributes,
  setUniforms,
  drawBufferInfo,
  resizeCanvasToDisplaySize,
  v3,
} from 'twgl.js';
import { GLElement } from './element';
import { GLObject } from './object';
import { degToRad, ColorEncoder } from '../utils';
import { Camera, OribitalCamera } from './camera';

export abstract class Scene extends GLElement {
  protected objects: {
    [key: string]: GLObject;
  } = {}

  protected objectsMap: Map<number, string>;

  protected camera: Camera;

  protected lightPosition: v3.Vec3;

  protected overlay?: HTMLDivElement;

  private runLoop = false;

  protected currentObjectName?: string;

  protected currentObject?: GLObject;

  private encoder: ColorEncoder;

  private lastObjectId = 0;

  inspectedObject?: GLObject;

  protected projectionOptions = {
    fov: 45,
    zNear: 0.5,
    zFar: 100,
  }

  constructor(gl: WebGLRenderingContext, overlay?: HTMLDivElement) {
    super();
    this.initialize(gl);
    this.overlay = overlay;
    this.camera = this.createCamera();
    this.lightPosition = [-10, 8, 0];
    this.encoder = new ColorEncoder(gl);
    this.objectsMap = new Map();
  }

  protected abstract createCamera(): Camera;

  protected abstract async setupObjects(): Promise<void>;

  protected abstract placeObjects(msec: number): void;

  protected isRunning = false;

  public async beginRenderLoop(): Promise<void> {
    this.onBeginRender();
    await this.setupObjects();

    if (this.overlay) {
      this.createOverlay();
    }

    this.runLoop = true;

    const render = (msec: number): void => {
      this.placeObjects(msec);
      this.renderScene(true);

      const pixel = new window.Uint8Array(4);
      this.gl.readPixels(
        this.gl.canvas.width / 2, this.gl.canvas.height / 2,
        1, 1, this.gl.RGBA, this.gl.UNSIGNED_BYTE, pixel,
      );
      this.currentObjectName = this.objectsMap.get(this.encoder.getId(pixel));
      if (this.currentObjectName) this.currentObject = this.objects[this.currentObjectName];

      this.renderScene(false);

      if (this.runLoop) {
        requestAnimationFrame(render);
      }
    };
    requestAnimationFrame(render);
  }

  public stopRenderLoop(): void {
    this.onStopRender();
    this.runLoop = false;
  }

  public beginInspect(object: GLObject): void {
    object.orbitalCamera = new OribitalCamera(object);
    this.inspectedObject = object;
  }

  public stopInspect(): void {
    if (this.inspectedObject) {
      const object = this.inspectedObject as GLObject;
      this.inspectedObject = undefined;
      object.orbitalCamera = undefined;
    }
  }

  protected onBeginRender(): void {
    this.isRunning = true;
  }

  protected onStopRender(): void {
    this.isRunning = false;
  }

  protected createOverlay(): void {
    if (this.overlay) {
      while (this.overlay.lastChild) {
        this.overlay.removeChild(this.overlay.lastChild);
      }
    }
  }

  protected async addObject<T extends GLObject>(
    name: string,
    object: T,
  ): Promise<T> {
    await object.initialize(this.gl);
    this.objects[name] = object;
    this.objects[name].id = this.lastObjectId;
    this.objectsMap.set(this.lastObjectId, name);
    this.lastObjectId += 1;
    return object;
  }

  protected getProjection(): m4.Mat4 {
    const canvas = (this.gl.canvas as HTMLCanvasElement);

    const fov = degToRad(this.projectionOptions.fov);
    const aspect = canvas.clientWidth / canvas.clientHeight;
    return m4.perspective(
      fov, aspect,
      this.projectionOptions.zNear, this.projectionOptions.zFar,
    );
  }

  protected getView(): m4.Mat4 {
    const camera = this.camera.toMatrix();
    return m4.inverse(camera);
  }

  protected renderScene(encodingMode = false): void {
    const { gl } = this;
    this.setupFrame();

    const projection = this.getProjection();
    const view = this.getView();
    const viewProjection = m4.multiply(projection, view);

    const globalUniforms = {
      uLightWorldPos: this.lightPosition,
      uLightColor: [1, 0.8, 0.8, 1],
      uAmbient: [0, 0, 0, 1],
      uSpecular: [1, 1, 1, 1],
      uShininess: 25,
      uSpecularFactor: 0.5,

      uViewInverse: m4.inverse(view),
    };

    let objectsToDraw = Array.from(this.objectsMap.values()).map((s) => this.objects[s]);
    objectsToDraw = objectsToDraw.filter((x) => !encodingMode || !x.isOverlay);

    if (this.inspectedObject) {
      gl.clearColor(0.25, 0.25, 0.25, 1);
      gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
      objectsToDraw = [this.inspectedObject];
    }

    objectsToDraw.forEach((object) => {
      const model = object.transform.toMatrix();

      const uniforms = {
        ...globalUniforms,
        uModel: model,
        uModelViewProjection: m4.multiply(viewProjection, model),
        uModelInverseTranspose: m4.transpose(m4.inverse(model)),
        uEncodingColor: this.encoder.getColor(object.id),
      };

      let programInfo = (encodingMode)
        ? object.encodingProgramInfo : object.programInfo;

      if (object === this.inspectedObject) {
        const fCamera = (object.orbitalCamera as Camera).toMatrix();
        const fView = m4.inverse(fCamera);
        const fViewProjection = m4.multiply(projection, fView);

        uniforms.uViewInverse = fCamera;
        uniforms.uModelViewProjection = m4.multiply(fViewProjection, model);

        programInfo = object.inspectingProgramInfo;
      } else if (object.isOverlay) {
        const canvas = (this.gl.canvas as HTMLCanvasElement);
        const aspect = canvas.clientWidth / canvas.clientHeight;

        m4.ortho(-aspect, aspect, 1, -1, -1, 1, uniforms.uModelViewProjection);
      }

      gl.useProgram(programInfo.program);
      setBuffersAndAttributes(gl, programInfo, object.bufferInfo);
      setUniforms(programInfo, { ...uniforms, ...object.uniforms });
      drawBufferInfo(gl, object.bufferInfo, object.getPrimitiveType());
    });
  }

  protected setupFrame(): void {
    const { gl } = this;

    resizeCanvasToDisplaySize(gl.canvas as HTMLCanvasElement);
    gl.viewport(0, 0, gl.canvas.width, gl.canvas.height);

    gl.clearColor(0.0, 0.0, 0.0, 1.0);
    gl.enable(gl.CULL_FACE);
    gl.enable(gl.DEPTH_TEST);
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
  }
}
