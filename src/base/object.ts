import {
  ProgramInfo, BufferInfo, createProgramInfo, ArraySpec,
} from 'twgl.js';
import { GLElement } from './element';
import { Transform } from './transform';

import EncodingFS from './shaders/encoding.frag';
import { OribitalCamera } from './camera';
import { Bounds } from '../utils';

export type Uniforms = {
  [key: string]: ArraySpec | WebGLTexture;
}

export abstract class GLObject extends GLElement {
  programInfo!: ProgramInfo;

  encodingProgramInfo!: ProgramInfo;

  inspectingProgramInfo!: ProgramInfo;

  bufferInfo!: BufferInfo;

  uniforms!: Uniforms;

  transform!: Transform;

  id!: number;

  orbitalCamera?: OribitalCamera;

  canBeInspected = true;

  isOverlay = false;

  bounds!: Bounds;

  async initialize(gl: WebGLRenderingContext): Promise<void> {
    super.initialize(gl);

    this.programInfo = createProgramInfo(this.gl, [
      this.getVS(), this.getFS()]);
    this.encodingProgramInfo = createProgramInfo(this.gl, [
      this.getVS(), EncodingFS]);
    if (this.canBeInspected && !this.isOverlay) {
      this.inspectingProgramInfo = createProgramInfo(this.gl, [
        this.getVS(), this.getInspectingFS()]);
    }

    this.bufferInfo = await this.createBufferInfo();
    this.uniforms = this.createUniforms();
    this.transform = new Transform();
  }

  abstract getVS(): string;

  abstract getFS(): string;

  getInspectingFS(): string {
    throw new Error('Not implemented');
  }

  abstract async createBufferInfo(): Promise<BufferInfo>;

  createUniforms(): Uniforms {
    return {};
  }

  getPrimitiveType(): number {
    return this.gl.TRIANGLES;
  }
}
